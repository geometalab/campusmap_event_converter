import csv
import re
from geojson import Feature, FeatureCollection, Point
import argparse

parser = argparse.ArgumentParser(description="Convert CSV to GeoJSON")
parser.add_argument("csv_path", help="Path to the CSV file to convert")
args = parser.parse_args()
csv_to_convert = args.csv_path

features = []
with open(csv_to_convert, newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    first_row = next(reader)
    for topic, nr, time, room, lat, lon, title, description, url in reader:
        lat, lon = map(float, (lat, lon))
        id_string = title.replace(" ", "").lower()[0:10]+ str(lat)[-4:-1] + str(lon)[-4:-1]
        id_string = re.sub(r'\W+', '', id_string)
        features.append(
            Feature(
                id='event/'+id_string,
                properties={
                    'id': "event/"+id_string,
                    'number': nr,
                    'name': title,
                    'place': room,
                    'time': time,
                    'category': topic,
                    'description': description,
                    'url': url,
                },
                geometry = Point((lon, lat))
            )

        )

collection = FeatureCollection(features)

with open("event.geojson", "w", newline='\n') as f:
    f.write('%s' % collection)
